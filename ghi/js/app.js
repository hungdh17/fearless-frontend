function createCard(name, description, pictureUrl, startDate, endDate, locationName) {
    return `
    <div class="col p-3">
        <div class="card shadow-lg p-3 mb-5 bg-body-tertiary rounded">
            <img src="${pictureUrl}" class="card-img-top" alt="...">
            <div class="card-body">
                <h5 class="card-title">${name}</h5>
                <h6 class="card-subtitle mb-2 text-body-secondary"><small class="text-muted">${locationName}</small></h6>
                <p class="card-text">${description}</p>
            </div>
            <div class="card-footer"><small class="text-muted">${String(startDate.getMonth()+1)}/${String(startDate.getDate()+1)}/${String(startDate.getFullYear())} - ${String(endDate.getMonth()+1)}/${String(endDate.getDate()+1)}/${String(endDate.getFullYear())}
            </small></div>  
        </div>
    </div>
    `;
  }

// window.addEventListener('DOMContentLoaded', async () => {

//     const url = 'http://localhost:8000/api/conferences/';
//     try{
//         const response = await fetch(url);
//         if(!response.ok){
//             console.log('Error: the url requested is not valid')
//         }

//         else {
//             const data = await response.json();
      
//             const conference = data.conferences[0];
//             const nameTag = document.querySelector('.card-title');
//             nameTag.innerHTML = conference.name;
      
//             const detailUrl = `http://localhost:8000${conference.href}`;
//             const detailResponse = await fetch(detailUrl);
//             if (detailResponse.ok) {
//             const details = await detailResponse.json();
//             const description = details.conference.description
//             const descriptionTag = document.querySelector('.card-text');
//             descriptionTag.innerHTML = details.conference.description
//             const imageTag = document.querySelector('.card-img-top');
//             imageTag.src = details.conference.location.picture_url;
//             }
//           }
//     }catch (e){
//         console.error('Error: ', e)
//     }
// });

window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/conferences/';
    try {
      const response = await fetch(url);
      if (!response.ok) {
        // what to do for this error?????
        const html = errorMessage();
        const column = document.querySelector('.row');
        column.innerHTML = html;
      } else {
        const data = await response.json();
        for (let conference of data.conferences) {
          const detailUrl = `http://localhost:8000${conference.href}`;
          const detailResponse = await fetch(detailUrl);
          if (detailResponse.ok) {
            const details = await detailResponse.json();
            const title = details.conference.name;
            const description = details.conference.description;
            const pictureUrl = details.conference.location.picture_url;
            const startDate = new Date(details.conference.starts);
            const endDate = new Date(details.conference.ends);
            const locationName = details.conference.location.name;
            const html = createCard(title, description, pictureUrl, startDate, endDate, locationName);
            const column = document.querySelector('.row');
            column.innerHTML += html;
            console.log(locationName);
          }
        }
      }
    } catch (e) {
      console.error('Error:', e);
      const errorContainer = document.querySelector('.error-container');
      errorContainer.innerHTML = `
        <div class="alert alert-danger" role="alert">
          An error occurred while loading conferences. Please try again later.
        </div>
      `;

    }
  });
