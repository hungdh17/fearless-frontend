// Get the cookie out of the cookie store
window.addEventListener('DOMContentLoaded', async () =>{
    const payloadCookie = await cookieStore.get('jwt_access_payload')
    if (payloadCookie) {
      // The cookie value is a JSON-formatted string, so parse it
      const encodedPayload = JSON.parse(payloadCookie.value);
    
      // Convert the encoded payload from base64 to normal string
      const decodedPayload = atob(encodedPayload)
    
      // The payload is a JSON-formatted string, so parse it
      const payload = JSON.parse(decodedPayload)
    // Check if "events.add_conference" is in the permissions.
    if (payload.user.perms.includes('events.add_conference')){
        removeDisplayNone("conference-link")
    }
    if (payload.user.perms.includes('events.add_location')){
        removeDisplayNone("location-link")
        }
    }
})

      // Print the payload
      console.log(payload);
    
      

      // If it is, remove 'd-none' from the link
    
    
      // Check if "events.add_location" is in the permissions.
      // If it is, remove 'd-none' from the link
    
    




function removeDisplayNone(target){
    const element = document.getElementById(target)
    element.classList.remove('d-none')
}