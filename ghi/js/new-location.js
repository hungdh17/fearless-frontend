window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/states/';
    const stateResponse = await fetch(url);
    // console.log(stateResponse);
    if (stateResponse.ok) {
      const data = await stateResponse.json();
      // console.log(data);
      const selectTag = document.getElementById('state');
      for (let state of data.states) {
        const option = document.createElement('option');
        option.value = state.abbreviation;
        option.innerHTML = state.name;
        selectTag.appendChild(option);
      }
  }
    const formTag = document.getElementById('create-location-form');
    formTag.addEventListener('submit', async event => {
      event.preventDefault();
      // console.log('is this working?');
      const formData = new FormData(formTag);
      const json = JSON.stringify(Object.fromEntries(formData));
      const fetchConfig = {
        method: "POST",
        body: json,
        headers: {
          'Content-Type': 'application/json',
        },
      };
  const locationUrl = 'http://localhost:8000/api/locations/';
  const response = await fetch(locationUrl, fetchConfig);
  if (response.ok) {
    formTag.reset();
    const newLocation = await response.json();

  }
});
});
