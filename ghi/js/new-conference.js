window.addEventListener('DOMContentLoaded', async () => {

    const locationUrl = 'http://localhost:8000/api/locations/';
    const locationResponse = await fetch(locationUrl);
    // console.log(stateResponse);
    if (locationResponse.ok) {
      const data = await locationResponse.json();
      // console.log(data);
      const selectTag = document.getElementById('location');
      for (let location of data.locations) {
        const option = document.createElement('option');
        option.value = location.id;
        option.innerHTML = location.name;
        selectTag.appendChild(option);
      }
  }
    const formTag = document.getElementById('create-conference-form');
    formTag.addEventListener('submit', async event => {
      event.preventDefault();
      // console.log('is this working?');
      const formData = new FormData(formTag);
      const json = JSON.stringify(Object.fromEntries(formData));
      const fetchConfig = {
        method: "POST",
        body: json,
        headers: {
          'Content-Type': 'application/json',
        },
      };
  const conferenceUrl = 'http://localhost:8000/api/conferences/';
  const response = await fetch(conferenceUrl, fetchConfig);
  if (response.ok) {
    formTag.reset();
    const newConference = await response.json();

  }
});
});
